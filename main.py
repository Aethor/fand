from dependencies import Dependency, GnaDependency, DatasetDependency

Dependency.resolve_or_die([GnaDependency(), DatasetDependency()])


import sys
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC
from gna.gna.config import Config
from logs import default_logger


if __name__ == "__main__":

    config = Config("./configs/definitions/baseline.json", sys.argv[1:])

    default_logger.verbosity = config["verbosity"]
    default_logger.log("info", f"running with config : {str(config)}")

    if not config["submit"] and not config["score"]:
        default_logger.log("info", "nothing to do. exitting...")
        exit()

    default_logger.log("info", "loading training datas...")
    raw_datas = pd.read_csv("./datas/train.csv").dropna()
    tfidf_vectorizer = TfidfVectorizer()
    X = tfidf_vectorizer.fit_transform(raw_datas["text"])
    y = raw_datas["label"]

    classifier = LinearSVC()

    if config["score"]:
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        default_logger.log("info", "training classifier on test datas...")
        classifier.fit(X_train, y_train)
        default_logger.log("info", "scoring classifier...")
        score = classifier.score(X_test, y_test)

        print(f"accuracy on testing set : {score}")

    if config["submit"]:
        default_logger.log("info", "training classifier on train datas...")
        classifier.fit(X, y)

        default_logger.log("info", "loading test datas...")
        submit_datas = pd.read_csv("./datas/test.csv")

        default_logger.log("info", "predicting on test datas...")
        predictions = classifier.predict(
            tfidf_vectorizer.transform(submit_datas["text"])
        )

        out_frame = pd.DataFrame({"id": submit_datas["id"], "label": predictions})
        out_frame.to_csv("./out/submission.csv", index=False)
        default_logger.log("info", "wrote ./out/submission.csv")
