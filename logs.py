from typing import Union


class Logger:
    log_verbosities = ["error", "warning", "info"]

    def __init__(self, verbosity: Union[int, str]):
        self._verbosity = verbosity

    @classmethod
    def __to_int_verbosity(cls, verbosity: str) -> int:
        return cls.log_verbosities.index(verbosity)

    @property
    def verbosity(self) -> int:
        return self._verbosity

    @verbosity.setter
    def verbosity(self, verbosity: Union[int, str]):
        if isinstance(verbosity, str):
            verbosity = Logger.__to_int_verbosity(verbosity)
        assert verbosity >= 0
        assert verbosity < len(Logger.log_verbosities)
        self._verbosity = verbosity

    def log(self, verbosity: Union[str, int], message: str):
        if isinstance(verbosity, str):
            verbosity = Logger.__to_int_verbosity(verbosity)
        if verbosity <= self.verbosity:
            print(f"[{Logger.log_verbosities[verbosity]}] {message}")


default_logger = Logger(2)
