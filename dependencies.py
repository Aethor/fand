from typing import List
import abc
import requests
import os
import sys
import zipfile

from logs import default_logger


class Dependency(abc.ABC):
    @abc.abstractmethod
    def check(self) -> bool:
        pass

    @abc.abstractmethod
    def solve(self) -> bool:
        pass

    @property
    @abc.abstractmethod
    def name(self) -> str:
        pass

    @classmethod
    def resolve_or_die(cls, dependencies: list):
        """
        :note: python 3.6 forbids current class reference in typing
        """
        default_logger.log("info", "resolving dependencies...")
        for dependency in dependencies:
            default_logger.log("info", f"resolving dependency {dependency.name}...")
            if not dependency.check():
                default_logger.log(
                    "info",
                    f"auto-detected missing {dependency.name} dependency. solving...",
                )
                if not dependency.solve():
                    default_logger.log(
                        "error", "could not resolve every dependency. exitting..."
                    )
                    exit()
        default_logger.log("info", "resolved all dependencies")


class DatasetDependency(Dependency):
    def check(self) -> bool:
        return (
            os.path.isfile("./datas/test.csv")
            and os.path.isfile("./datas/train.csv")
            and os.path.isfile("./datas/submit.csv")
        )

    def solve(self) -> bool:
        default_logger.log("info", "trying to download dataset...")
        try:
            from google_drive_downloader import GoogleDriveDownloader as gdd

            gdd.download_file_from_google_drive(
                file_id="1RqNtEh_1c-vINcwTqHjiU7qo808v29P8",
                dest_path="./datas/datas.zip",
                unzip=True,
            )
            default_logger.log("info", "successfully downloaded dataset")
            return True
        except Exception as e:
            print(sys.stderr, e)
            default_logger.log("error", "could not download dataset.")
            return False

    @property
    def name(self) -> str:
        return "DATASET"


class GnaDependency(Dependency):
    def check(self) -> bool:
        return os.path.isdir("./gna/gna")

    def solve(self) -> bool:
        try:
            os.system("git submodule update --init --recursive")
            return True
        except Exception as e:
            print(sys.stderr, e)
            default_logger.log("error", "could not download the gna library.")
            return False

    @property
    def name(self) -> str:
        return "GNA_LIB"


all_dependencies: List[Dependency] = [
    DatasetDependency(),
    GnaDependency(),
]
