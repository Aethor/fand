---
title: "FAND : Fake News Detector"
--- 

# Introduction

FAND is a simple fake news detector, realised for [**this kaggle competition**](https://www.kaggle.com/c/smm-hw2-fakenewsdetecion/overview).


# Getting up and running

First clone the repository :

```sh
git clone --recurse-submodules https://gitlab.com/Aethor/fand.git
```

Then, install the requirements :

```sh
pip install -r requirements.txt
```

Other dependencies should be fetched automatically when needed by the scripts (altough you need *git* in your path for this to work).


# Running 

The `main.py` file has 2 actions associated with it :

- `python3 main.py --score` will compute the accuracy of the fake news detection by using a part of the dataset as training data
- `python3 main.py --submit` will export a submission for the kaggle competition as *./out/submission.csv*


# Principles

## Datas

The dataset contains news article with the following fields :

- id : unique ID of the sample
- title : title of the news article
- author : author of the article
- text : text of the article
- label : 1 for unreliable news, 0 for unreliable one


## Experiments

The **notebook.ipynb** file contains a jupyer notebook with experiments. The goal was to test different features and different classifier. We mainly tested two ways of extracting features, while using only the provided *text* field :

- tf-idf : We represent each sample using a vector computed using tf-idf. In this vector, which has the size of our vocabulary, each entry $i$ corresponds to the tf-idf score of the $i^{th}$ word of the vocabulary
- W2V weighted by tf-idf : We represent each sample using a single vector, computed as a weighted average of its word vectors. The weight are obtained using the tf-idf score of each word. The word vectors are trained using the corpus : we also tried using pretrained word vectors, but the results were subpar.

We also experimented with different classifiers to see which one could get us the best results.

![Final results of our experiments](./assets/scores.jpg)


## Results

In our previously described experiments, we saw that the tf-idf representation of documents combined with a linear svm classifier was the best combination of all those we tested. Using this combination, we report an accuracy of **0.96750**.

![Leaderboard screenshot](./assets/leaderboard.png)
